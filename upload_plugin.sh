#!/bin/bash

zip community-events.zip *
scp community-events.zip 192.168.1.82:~/
ssh 192.168.1.82 "sudo -u www-data wp --path=/var/www/wordpress plugin install --force ~/community-events.zip"
rm community-events.zip
