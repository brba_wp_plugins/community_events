<h1><?php esc_html_e( "Manage Community Events", "cev"); ?></h1>

<?php

global $wpdb;
global $table_name;

if( ! class_exists( 'WP_List_Table' ) ) {
    require_once( ABSPATH . 'wp-admin/includes/class-wp-list-table.php' );
}

if (isset($_GET['action'])) {
    if ($_GET['action'] == 'delete') { // delete event
        // get ID to delete
        $id = $_GET['id'];

        $wpdb->delete(
            $table_name,
            array(
                "id" => $id
            )
        );

    }

    elseif ($_GET['action'] == 'verify') {
        $id = $_GET['id'];

        $wpdb->update(
            $table_name,
            array( //update 
                "verified" => true
            ),
            array('id' => $id), // where
            array("%d"), // column value types
            array("%d") // where (id) type
        );

    }

    elseif ($_GET['action'] == 'unverify') {
        $id = $_GET['id'];

        $wpdb->update(
            $table_name,
            array( //update 
                "verified" => false
            ),
            array('id' => $id), // where
            array("%d"), // column value types
            array("%d") // where (id) type
        );

    }

}


class CommunityEventsListTable extends WP_List_Table {

    function get_columns() {
        $columns = array(
            "title" => "Title",
            "description" => "Description",
            "verified" => "Verified",
        );

        return $columns;
    }

    function get_sortable_columns() {
        $sortable_columns = array(
            "title" => array("title", false),
            "description" => array("description", false),
            "verified" => array("verified", true)
        );

        return $sortable_columns;
    }

    function sort_reorder() {

        global $table_name;
        global $wpdb;

        $orderby = ( ! empty($_GET['orderby']) ) ? $_GET['orderby'] : 'title';
        $order = ( ! empty($_GET['order']) ) ? $_GET['order'] : 'asc';

        return $wpdb->get_results("SELECT * FROM $table_name ORDER BY $orderby $order"); // SQL INJECTION?

    }

    function prepare_items() {

        global $wpdb;
        global $table_name;

        $columns = $this->get_columns();

        $hidden_vals = array();
        $sortable = $this->get_sortable_columns();

        $this->_column_headers = array($columns, $hidden_vals, $sortable);

        // $this->items = $wpdb->get_results("SELECT * FROM $table_name");
        $this->items = $this->sort_reorder();
    }

    function column_default($item, $column_name) {
        switch ($column_name) {
        case "title":
            // TODO Make display of this bigger
            return $item->title;
        case "description":
            return $item->description;
        case "verified":
            $page = $_REQUEST['page'];
            if (!$item->verified) {
                return "<a href='?page=$page&action=verify&id=$item->id'>
                          <button class='button button-primary'>Verify</button>
                       </a>";
            }
            else {
                return "<a href='?page=$page&action=unverify&id=$item->id'><button class='button button-primary'>Unverify</button></a>";
            }

        default:
            return print_r( $item, true );


        }
    }

    function column_title($item) {

        $page = $_REQUEST['page'];

        $actions = array(
            "delete" => "<a href='?page=$page&action=delete&id=$item->id'>Delete</a>"
        );

        return sprintf("%s %s", $item->title, $this->row_actions($actions));
    }


}

$cevListTable = new CommunityEventsListTable();
$cevListTable->prepare_items();
$cevListTable->display(); 
