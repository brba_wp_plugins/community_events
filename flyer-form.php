<div id="includes">
    <title>Submit a Flyer for the Board</title>
    <link rel="stylesheet"  type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans">

    <link rel="stylesheet" href="<?php echo plugin_dir_url(__FILE__) . '/community-events' ?>/flyer-form.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="<?php echo plugin_dir_url(__FILE__) .'/community-events' ?>/flyer-form.js"></script>

    <style>
     #init-buttons {
         margin-top: 24vh;
         background-color: white;
         border-radius: 25px;
         width: 60%;
         padding: 50px;
         margin-left: 15%;
     }

     #init-buttons > button{
         width: 200px;
     }
    </style>
</div>

<div id="init-buttons">
    <center> <button onclick = "function1()" class="button-large-green">Input Event Info</button></center>
</div>

<form id="upload-words">
    <h2 id="stupid">Add Your Local Event Flyer</h2>
        <div class="question">
            <input type="text" name="title" required />
            <label>Event Name</label>
        </div>
        <div class="question">
            <?php wp_editor("", "description") ?>
        </div>

        <button  class="button-med-green">Submit</button>

</form>


<script>
function function1() {
    $("#init-buttons").fadeOut();
    $("#upload-words").fadeIn();
  }

  function function2() {
    $("#init-buttons").fadeOut();
    $("#upload-flyer").fadeIn();
  }

</script>

