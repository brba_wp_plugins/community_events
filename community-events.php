<?php 



/**
 * Plugin Name: BRBA Community Events
 * Description: Allows people to create community events.
 * Version: 0.1
 * Author: Ramesh Balaji
 */

// plugin config
global $wpdb;

$table_name = $wpdb->prefix."community_events";
$db_ver = "new";
$current_db_ver = get_option("cev_db_ver");

if ($current_db_ver != $db_ver) {
    // Create or update table
    $charset_collate = $wpdb->get_charset_collate();
    $sql = "CREATE TABLE $table_name
            (id INT PRIMARY KEY AUTO_INCREMENT NOT NULL,
             title VARCHAR(200) NOT NULL,
             description TEXT NOT NULL,
             verified BOOL
            ) $charset_collate";

    // Execute the query
    require_once(ABSPATH . "wp-admin/includes/upgrade.php");
    dbDelta($sql);

    // update db version
    update_option("cev_db_ver", $db_ver);
}




function create_cev_func( $atts ) {



    global $wpdb;
    global $table_name;
    // Check if POST variables are defined to only execute this if the user is trying to submit something

    if (isset($_POST['title'])) {

        // For the text input we get 'title' and 'information'
        // TODO: Add form sanitation

        $title = $_POST["title"];
        $description = stripslashes($_POST["description"]); // strip slashes for images

        $wpdb->insert(
            $table_name,
            array(
                'title' => $title,
                'description' => $description,
                'verified' => false
            )
        );
    }

    ob_start();
    include 'create_form.php';
    return ob_get_clean();

}

function add_cev_manage_page() {
    add_menu_page(
        __("Manage Community Events", "cev"),
        "Community Events",
        "manage_options",
        "community-events/manage_events.php"
    );
}

function view_cev_gen() {

    global $wpdb;
    global $table_name;

    $events = $wpdb->get_results("SELECT * FROM $table_name WHERE verified=1");


    ob_start();
    include 'view_cev.php';
    return ob_get_clean();
}

add_shortcode( 'create_cev', 'create_cev_func' );
add_shortcode( 'view_cev', 'view_cev_gen');
// Add admin section
add_action('admin_menu', "add_cev_manage_page");
